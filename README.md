# CPM 2015 - 2016
## Redes de Computadores
### SEDUC/TO 
![CPM](logocpm.jpg)


Neste repositório adicionei conteúdo, avaliações e exercícios ministrados no curso técnico em redes de computadores.

## Disciplinas lecionadas

* Análise e Projeto de Sistemas
* Empreendedorismo
* Lógica de Programação
* Sistemas Operacionais 
* Software Livre

# Observação

O conteúdo disponibilizado foi utilizado em um **_curso técnico integrado ao ensino médio_**. 

Na disciplina Software Livre foi focado as discussões filosóficas e não técnicas sobre software. Lembrando que linhas de comando é comum na maioria dos sistemas operacionais. Eu prefiro usar o termo GNU/Linux. 

O [Guia Foco](https://guiafoca.org/) é muito mais completo e indicado, inclusive aborda os conceitos comentados na sala de aula.

O sítio [Portal do Software Público](https://www.gov.br/governodigital/pt-br/software-publico/software-de-governo/software-de-governo) 
não apresentava mais os softwares comentados nas aulas.  

 

